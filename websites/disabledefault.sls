apache.a2dissite:
  module.run:
    - args: 000-default

restartapache:
  module.run:
    - name: service.full_restart
    - args: apache2
