/srv/www:
  file.directory:
    - user: www-data
    - group: www-data

/srv/www/example.com:
  file.directory:
    - user: www-data
    - group: www-data

touchdafile:
  file.touch:
    - name: /srv/www/example.com/index.html

appenddafile:
  file.append:
    - name: /srv/www/example.com/index.html
    - text: "Welcome to FooTennessee"
